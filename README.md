<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

<!-- Update the below line with your artifact name -->
# Get Value from JSON Pointer

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)


## Overview
This transformation takes an object and a json pointer  It gets a value out of the object based on the JSON Pointer you give it, otherwise it returns undefined. Since a json file can be nested with a lot of information, the json pointer provides a path through the json to get a value. 

This can be compared to a file system (json file), where a path (json pointer) can direct you through a file system to the file you want to open (the value you want returned by the pointer).


## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`

## How to Install

To install the Pre-Built Transformation:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built Transformation:

1. Once the JST is installed as outlined in the How to Install section above, navigate to the section in your workflow where you would like to convert a string to an object and add a JSON Transformation task.
2. Inside the Transformation task, search for and select `getValueFromPointer` (the name of the internal JST).
3. In the jsonPointer field, create a json pointer to the value you want returned.
4. In the obj field, enter the valid json.
5. Run the transformation.

### Examples

**Example 1** 

Consider you have the json:
```
{ "a": 
    {
     "b": 2, 
     "c": 3
    }
}
```
And the json pointer: `"/a/b"`
Running this through the transformation would return 2
Running `"/a/c"` would return 3
Running `"/a"` would return
```
{
     "b": 2, 
     "c": 3
}
```
A link to the json pointer ref can be found [here](https://tools.ietf.org/html/rfc6901)

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
## Additional Information
Please use your Itential Customer Success account if you need support when using this Pre-Built.
